package net.togogo.dao;

import net.togogo.bean.Bill;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface BillMapper {
    List<Bill> selectBillAll();

    int selectBillCount(@Param("bill")Bill bill);

    List<Bill> selectBillByLimit(@Param("pageNo") int pageNo,
                                 @Param("pageSize") int pageSize,
                                 @Param("bill") Bill bill);

    boolean insertBill(@Param("bill")Bill bill);

    Bill selectBillById(String id);

    int updateBill(@Param("bill") Bill bill);
}
