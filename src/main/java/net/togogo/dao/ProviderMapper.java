package net.togogo.dao;

import net.togogo.bean.Bill;
import net.togogo.bean.Provider;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ProviderMapper {
    int selectProviderCount(@Param("provider") Provider provider);

    List<Provider> selectProviderByLimit(@Param("pageNo") int pageNo,
                                 @Param("pageSize") int pageSize,
                                 @Param("provider") Provider provider);

    Provider selectProviderById(String id);

    int updateProvider(@Param("provider") Provider provider);

    int addProvider(@Param("provider") Provider provider);
}
