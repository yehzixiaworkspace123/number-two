package net.togogo.dao;

import net.togogo.bean.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserMapper {
    List<User> selectUserAll();

    User selectUserByUserCodeAndPWD(User user);

    int selectUserCount(@Param("user") User user);

    List<User> selectUserCountByLimit(@Param("pageNo") int pageNo, @Param("pageSize") int pageSize, @Param("user") User user);

    User selectUserById(String id);

    int updateUser(@Param("user") User user );

    int addUser(@Param("user") User user );

    User selectUserByIdAndPassword(@Param("userCode") String userCode, @Param("userPassword") String oldPass);

    int updatePassword(@Param("user") User user);
}
