package net.togogo.controller;

import com.alibaba.fastjson.JSON;
import net.togogo.bean.User;
import net.togogo.service.UserService;
import net.togogo.util.Contra;
import net.togogo.util.PageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/user")
public class UserController {


    @Autowired
    @Qualifier("userService")
    private UserService userService;

    @RequestMapping("/userList")
    public String billList(Model model, HttpSession session,
                           @RequestParam(value = "pageNo", required = false) String pageNos,
                           User user) {
        //System.out.println("页面传递进来的值=======>"+bill.toString());

        int pageNo = 1;
        int pageSize = 3;
        //查询出user有多少条数据
        int userCount = userService.selectUserCount(user);
        PageUtil pageUtil = new PageUtil();
        pageUtil.setPageNo(pageNo);
        pageUtil.setPageSize(pageSize);
        pageUtil.setTotalCount(userCount);
        int totalPageCount = pageUtil.getTotalPageCount();
        if (pageNos != null) {
            int pos = Integer.parseInt(pageNos);
            if (pos <= 0) {
                pageNo = 1;
            } else if (pos > totalPageCount) {
                pageNo = totalPageCount;
            } else {
                pageNo = pos;
            }
            if (totalPageCount == 0) {
                pageNo = 1;
            }
        }

        //进行分页查询
        List<User> userList = userService.selectUserCountByLimit(pageNo, pageSize, user);
        model.addAttribute("userList", userList);
        Map map = new HashMap();
        map.put("pageNo", pageNo);
        map.put("pageSize", pageSize);
        map.put("totalCount", userCount);
        map.put("totalPageCount", totalPageCount);
        model.addAttribute("pages", map);
        //回显参数  通过@ModelAttribute进行改进
        Map paramMap = new HashMap();
        paramMap.put("userName",user.getUserName());
        paramMap.put("userRole",user.getUserRole());
        model.addAttribute("paramMap",paramMap);
        return "userList";
    }

    //{id} 占位符
    @RequestMapping("/userShow/{id}")
    @ResponseBody
    public String userShow(@PathVariable String id, Model model){

        System.out.println("页面传递来的ID======>"+id);

        User user = userService.selectUserById(id);
        if (user==null){
            return null;
        }
        return JSON.toJSONStringWithDateFormat(user,"yyyy-MM-dd");
    }


    @PostMapping("/userUpdate")
    @ResponseBody
    public String userUpdate(User user,HttpSession session){

        System.out.println(user);


        User loginUser = (User)session.getAttribute(Contra.USER_SESSION);
        if (loginUser!=null){
            Integer userId = loginUser.getId();
            user.setModifyBy(userId);
        }

        int i = userService.updateUser(user);
        if (i!=1){
            return JSON.toJSONString("error");
        }
        return JSON.toJSONString("200");
    }


    @PostMapping("userAdd")
    @ResponseBody
    public String userAdd(@RequestBody User user,HttpSession session){
        if (user == null){
            return JSON.toJSONString("表单数据有误");
        }

        User loginUser = (User)session.getAttribute(Contra.USER_SESSION);
        if (loginUser!=null){
            Integer userId = loginUser.getId();
            user.setCreatedBy(userId);
        }

        int i = userService.addUser(user);
        String result = JSON.toJSONString(i);
        return result;
    }

}
