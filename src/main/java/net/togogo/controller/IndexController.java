package net.togogo.controller;

import com.alibaba.fastjson.JSON;
import net.togogo.bean.User;
import net.togogo.service.UserService;
import net.togogo.util.Contra;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/index")
public class IndexController {

    @Autowired
    @Qualifier("userService")
    private UserService userService;


    @RequestMapping("/login")
    public String login(){
        return "login";
    }

    @RequestMapping("/doLogin")
    public String doLogin(User user, HttpSession session, Model model){
        //进行登录
        User users = userService.selectUserByUserCodeAndPWD(user);
        if(users != null){//代表用户存在,账号密码正确
            //将用户信息存到session中.
            session.setAttribute(Contra.USER_SESSION,users);
            //进行页面跳转
            return  "index";
        }
        //登录失败
        model.addAttribute("message","用户名或者密码错误");
        //转发
        return "forward:/index/login";
    }

    //RequestBody 接收请求的json类型的参数
    //ResponseBody 返回json类型的数据
    @RequestMapping("/doLoginJS")
    @ResponseBody
    public String doLoginJS(@RequestBody User user,HttpSession session){
        System.out.println("ajax接收到的参数=========>"+user.toString());
        Map map = new HashMap();
        //进行登录
        User users = userService.selectUserByUserCodeAndPWD(user);
        if(users != null){//代表用户存在,账号密码正确
            //将用户信息存到session中.
            session.setAttribute(Contra.USER_SESSION,users);
            map.put("message","true");
        }else{
            map.put("message","false");
        }
        return JSON.toJSONString(map);
    }


    @RequestMapping("/toIndex")
    public String toIndex(){
        return  "index";
    }

    //退出登录
    @RequestMapping("/loginOut")
    public String loginOut(HttpSession session){
        //session失效
        session.invalidate();
        //页面跳转回登录页
        return "redirect:/index/login";
    }

    @RequestMapping("pw")
    public String pw(){
        return "pw";
    }

    @PostMapping("updatePW")
    @ResponseBody
    public String updatePW(@RequestBody Map<String,String> map,HttpSession session){
        String newPass = map.get("pass");
        String checkPass = map.get("checkPass");
        String oldPass = map.get("oldPass");

        User loginUser = (User)session.getAttribute(Contra.USER_SESSION);
        if(loginUser == null){
            //無session則是未登录狀態
            System.out.println("============未登录，請重新登录=======");
            return "redirect:/index/login";
        }

        if (!newPass.equals(checkPass)){
            return JSON.toJSONString("密码不一致");
        }

        //查询原密码是否正确
        User user = userService.selectUserByIdAndPassword(loginUser.getUserCode(),oldPass);
        if (user==null){
            return JSON.toJSONString("原密码错误");
        }

        user.setUserPassword(newPass);
        int result = userService.updatePassword(user);
        if (result==1){
            session.invalidate();
            return JSON.toJSONString("200");
        }
        return null;
    }
}
