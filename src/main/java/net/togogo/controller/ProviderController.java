package net.togogo.controller;

import com.alibaba.fastjson.JSON;
import net.togogo.bean.Bill;
import net.togogo.bean.Provider;
import net.togogo.bean.User;
import net.togogo.service.BillService;
import net.togogo.service.ProviderService;
import net.togogo.util.Contra;
import net.togogo.util.PageUtil;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.math.RandomUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/provider")
public class ProviderController {

    @Autowired
    @Qualifier("providerService")
    ProviderService providerService;

    @RequestMapping("/providerList")
    public String billList(Model model, HttpSession session,
                           @RequestParam(value = "pageNo", required = false) String pageNos,
                           Provider provider) {
        //System.out.println("页面传递进来的值=======>"+bill.toString());

        int pageNo = 1;
        int pageSize = 2;
        //查询出provider有多少条数据
        int providerCount = providerService.selectProviderCount(provider);
        PageUtil pageUtil = new PageUtil();
        pageUtil.setPageNo(pageNo);
        pageUtil.setPageSize(pageSize);
        pageUtil.setTotalCount(providerCount);
        int totalPageCount = pageUtil.getTotalPageCount();
        if (pageNos != null) {
            int pos = Integer.parseInt(pageNos);
            if (pos <= 0) {
                pageNo = 1;
            } else if (pos > totalPageCount) {
                pageNo = totalPageCount;
            } else {
                pageNo = pos;
            }
            if (totalPageCount == 0) {
                pageNo = 1;
            }
        }

        //进行分页查询
        List<Provider> providerList = providerService.selectProviderCountByLimit(pageNo, pageSize, provider);
        model.addAttribute("providerList", providerList);
        Map map = new HashMap();
        map.put("pageNo", pageNo);
        map.put("pageSize", pageSize);
        map.put("totalCount", providerCount);
        map.put("totalPageCount", totalPageCount);
        model.addAttribute("pages", map);
        //回显参数  通过@ModelAttribute进行改进
        Map paramMap = new HashMap();
        paramMap.put("proName",provider.getProName());
        paramMap.put("proContact",provider.getProContact());
        model.addAttribute("paramMap",paramMap);
        return "providerList";
    }


    //{id} 占位符
    @RequestMapping("/providerShow/{id}")
    @ResponseBody
    public String billShow(@PathVariable String id,Model model){

        System.out.println("页面传递来的ID======>"+id);

        Provider provider = providerService.selectProviderById(id);
        if (provider==null){
            return null;
        }
        return JSON.toJSONStringWithDateFormat(provider,"yyyy-MM-dd HH:mm:ss");
    }


    @PostMapping("/providerUpdate")
    @ResponseBody
    public String providerUpdate(Provider provider,HttpSession session){

        System.out.println(provider);


        User loginUser = (User)session.getAttribute(Contra.USER_SESSION);
        if (loginUser!=null){
            Integer userId = loginUser.getId();
            provider.setModifyBy(userId);
        }

        int i = providerService.updateProvider(provider);
        if (i!=1){
            return JSON.toJSONString("error");
        }
        return JSON.toJSONString("200");
    }

    @PostMapping("providerAdd")
    @ResponseBody
    public String providerAdd(@RequestBody Provider provider,HttpSession session){
        if (provider == null){
            return JSON.toJSONString("表单数据有误");
        }

        User loginUser = (User)session.getAttribute(Contra.USER_SESSION);
        if (loginUser!=null){
            Integer userId = loginUser.getId();
            provider.setCreatedBy(userId);
        }

        int i = providerService.addProvider(provider);
        String result = JSON.toJSONString(i);
        return result;
    }

}

