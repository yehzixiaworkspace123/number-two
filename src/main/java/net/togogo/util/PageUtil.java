package net.togogo.util;
//分页工具类
public class PageUtil {

    //当前页数
    private int pageNo;
    //页面大小
    private int pageSize;
    //总个数
    private int totalCount;
    //总页数
    private int totalPageCount;

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
        //得到总个数之后可以求出总页数
        this.getTotalPageCountDS();
    }

    public int getTotalPageCount() {
        return totalPageCount;
    }

    public void setTotalPageCount(int totalPageCount) {
        this.totalPageCount = totalPageCount;
    }

    public void getTotalPageCountDS(){
        //进行总页数的计算
        if(this.getTotalCount()%this.getPageSize()==0){
            this.totalPageCount=this.getTotalCount()/this.getPageSize();
        }else if(this.getTotalCount()%this.getPageSize()!=0){
            this.totalPageCount=this.getTotalCount()/this.getPageSize()+1;
        }else{
            this.totalPageCount=1;
        }
    }
}
