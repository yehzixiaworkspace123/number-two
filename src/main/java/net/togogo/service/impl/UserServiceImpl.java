package net.togogo.service.impl;

import net.togogo.bean.User;
import net.togogo.dao.UserMapper;
import net.togogo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service("userService")
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public List<User> selectUserAll() {
        return userMapper.selectUserAll();
    }

    @Override
    public User selectUserByUserCodeAndPWD(User user) {
        return userMapper.selectUserByUserCodeAndPWD(user) ;
    }

    @Override
    public int selectUserCount(User user) {
        int count = userMapper.selectUserCount(user);
        return count;
    }

    @Override
    public List<User> selectUserCountByLimit(int pageNo, int pageSize, User user) {
        pageNo = (pageNo-1)*pageSize;
        List<User> userList = userMapper.selectUserCountByLimit(pageNo,pageSize,user);
        return userList;
    }

    @Override
    public User selectUserById(String id) {
        return userMapper.selectUserById(id);
    }

    @Override
    public int updateUser(User user) {
        user.setModifyDate(new Date());
        int i = userMapper.updateUser(user);
        return i;
    }

    @Override
    public int addUser(User user) {
        user.setCreationDate(new Date());
        return userMapper.addUser(user);
    }

    @Override
    public User selectUserByIdAndPassword(String userCode, String oldPass) {
        return userMapper.selectUserByIdAndPassword(userCode,oldPass);
    }

    @Override
    public int updatePassword(User user) {
        user.setModifyDate(new Date());
        return userMapper.updatePassword(user);
    }
}
