package net.togogo.service.impl;

import net.togogo.bean.Bill;
import net.togogo.dao.BillMapper;
import net.togogo.dao.UserMapper;
import net.togogo.service.BillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service("billService")
public class BillServiceImpl  implements BillService {

    @Autowired
    BillMapper billMapper;

    @Override
    public List<Bill> selectBillAll() {
        return billMapper.selectBillAll();
    }

    @Override
    public int selectBillCount(Bill bill) {
        return billMapper.selectBillCount(bill);
    }

    @Override
    public List<Bill> selectBillByLimit(int pageNo, int pageSize,Bill bill) {
        pageNo = (pageNo-1)*pageSize;
        return billMapper.selectBillByLimit(pageNo,pageSize,bill);
    }

    @Override
    public boolean insertBill(Bill bill) {
        return billMapper.insertBill(bill);
    }

    @Override
    public Bill selectBillById(String id) {
        Bill bill = billMapper.selectBillById(id);
        return bill;
    }

    @Override
    public int updateBill(Bill bill) {
        bill.setModifyDate(new Date());

        int result = billMapper.updateBill(bill);

        return result;
    }
}
