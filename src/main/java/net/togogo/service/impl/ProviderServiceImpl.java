package net.togogo.service.impl;

import net.togogo.bean.Provider;
import net.togogo.bean.User;
import net.togogo.dao.ProviderMapper;
import net.togogo.service.ProviderService;
import net.togogo.util.Contra;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @author Junko
 * @date 2020/11/23 10:56
 **/
@Service("providerService")
public class ProviderServiceImpl implements ProviderService {

    @Autowired
    ProviderMapper providerMapper;

    @Override
    public int selectProviderCount(Provider provider) {
        int count = providerMapper.selectProviderCount(provider);
        return count;
    }

    @Override
    public List<Provider> selectProviderCountByLimit(int pageNo, int pageSize, Provider provider) {
        pageNo = (pageNo-1)*pageSize;
        List<Provider> providerList = providerMapper.selectProviderByLimit(pageNo, pageSize, provider);
        return providerList;
    }

    @Override
    public Provider selectProviderById(String id) {
        Provider provider = providerMapper.selectProviderById(id);
        return provider;
    }

    @Override
    public int updateProvider(Provider provider) {
        provider.setModifyDate(new Date());
        int result = providerMapper.updateProvider(provider);
        return result;
    }

    @Override
    public int addProvider(Provider provider) {

        provider.setCreationDate(new Date());

        int i = providerMapper.addProvider(provider);
        return i;
    }
}
