package net.togogo.service;

import net.togogo.bean.Provider;

import java.util.List;

/**
 * @author Junko
 * @date 2020/11/23 10:56
 **/
public interface ProviderService {
    int selectProviderCount(Provider provider);

    List<Provider> selectProviderCountByLimit(int pageNo, int pageSize, Provider provider);

    Provider selectProviderById(String id);

    int updateProvider(Provider provider);

    int addProvider(Provider provider);
}
