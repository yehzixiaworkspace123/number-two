package net.togogo.service;

import net.togogo.bean.Bill;

import java.util.List;

public interface BillService {
    List<Bill> selectBillAll();

    int selectBillCount(Bill bill);

    List<Bill> selectBillByLimit(int pageNo, int pageSize,Bill bill);

    boolean insertBill(Bill bill);

    /**
     * 根据id查询账单详情
     * @param id
     * @return
     */
    Bill selectBillById(String id);

    int updateBill(Bill bill);

}
