package net.togogo.service;

import net.togogo.bean.User;

import java.util.List;

public interface UserService {
    List<User> selectUserAll();

    User selectUserByUserCodeAndPWD(User user);

    int selectUserCount(User user);

    List<User> selectUserCountByLimit(int pageNo, int pageSize, User user);

    User selectUserById(String id);

    int updateUser(User user);

    int addUser(User user);

    User selectUserByIdAndPassword(String userCode, String oldPass);

    int updatePassword(User user);
}
