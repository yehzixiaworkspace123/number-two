<%@ include file="comment/jspHead.jsp"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>系统登录 - 超市账单管理系统</title>
    <link rel="stylesheet" href="${pageContext.servletContext.contextPath}/static/css/style.css"/>
</head>
<body class="login_bg">
    <section class="loginBox">
        <header class="loginHeader">
            <h1>超市账单管理系统</h1>
        </header>
        <section class="loginCont">
            <form class="loginForm" method="post" action="${pageContext.servletContext.contextPath}/index/doLogin">
                <div class="inputbox">
                    <label for="user">用户名：</label>
                    <input id="user" type="text" name="userCode" placeholder="请输入用户名" required/>
                </div>
                <div class="inputbox">
                    <label for="mima">密码：</label>
                    <input id="mima" type="password" name="userPassword" placeholder="请输入密码" required/>
<%--                ${message}--%>
                    <span id="message"></span>
                </div>
                <div class="subBtn">
                    <input type="button" value="登录" />
                    <input type="reset" value="重置"/>
                </div>
            </form>
        </section>
    </section>
</body>
<script src="${pageContext.servletContext.contextPath}/static/js/jquery.js"></script>
<script>
    $(function () {
        $("[type='button']").click(function () {
            //获取页面的数据
           var $user =  $("#user").val();
           var $mima =  $("#mima").val();
           //进行ajax登录
            $.ajax({
                contentType:"application/json",
                url:"${pageContext.servletContext.contextPath}/index/doLoginJS",
                data:JSON.stringify({"userCode":$user,"userPassword":$mima,birthday:"1992-12-12"}),
                type:"post",
                dataType:"json",
                success:function (data) {
                    if(data.message=="true"){
                        location.href="${pageContext.servletContext.contextPath}/index/toIndex";
                    }else{
                        $("#message").html("账号或者密码错误");
                    }
               }
            });
        });
    });


</script>

</html>