﻿<%@ include file="comment/jspHead.jsp" %>
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>超市账单管理系统</title>
    <link rel="stylesheet" href="${pageContext.servletContext.contextPath}/static/css/public.css"/>
    <link rel="stylesheet" href="${pageContext.servletContext.contextPath}/static/css/style.css"/>


</head>
<body>

<jsp:include page="comment/head.jsp"></jsp:include>

<!--主体内容-->
<section class="publicMian ">
    <jsp:include page="comment/left.jsp"></jsp:include>
    <div class="right">
        <div class="location">
            <strong>你现在所在的位置是:</strong>
            <span>供应商管理页面</span>
        </div>

        <form method="get" action="${pageContext.servletContext.contextPath}/user/userList">
            <div class="search">
                <span>用户名：</span>
                <input type="text" name="userName" value="${paramMap.userName}" placeholder="请输入用户名称"/>


                <span>用户角色：</span>
                <select name="userRole">
                    <option value="">--请选择--</option>
                    <option value="1">系统管理员</option>
                    <option value="2">经理</option>
                    <option value="3">普通员工</option>
                </select>

                <input type="hidden" id="thisRole" value="${paramMap.userRole}">
                <input type="submit" id="aa" value="查询"/>

            </div>
        </form>

        <div id="app">
            <!-- Form -->
            <el-button type="text" @click="dialogFormVisible = true">添加供应商</el-button>
            <el-dialog title="添加用户" :visible.sync="dialogFormVisible">
                <el-form :model="form" :rules="rules" ref="form">
                    <el-form-item label="用户编号" :label-width="formLabelWidth" prop="userCode">
                        <el-input placeholder="请输入编号" v-model="form.userCode" autocomplete="off"></el-input>
                        <%--                            <input name="asd" id="asd" value=""/>--%>
                    </el-form-item>
                    <el-form-item label="用户名称" :label-width="formLabelWidth" prop="userName">
                        <el-input placeholder="请输入名称" v-model="form.userName" autocomplete="off"></el-input>
<%--                            <input name="asd" id="asd" value=""/>--%>
                    </el-form-item>

                    <el-form-item label="用户密码" :label-width="formLabelWidth" prop="userPassword">
                    <el-input v-model="form.userPassword" placeholder="请输入密码" v-model="input" show-password></el-input>
                    </el-form-item>

                    <el-form-item label="性别"  :label-width="formLabelWidth" prop="gender">
                        <el-radio-group v-model="form.gender" size="medium">
                            <el-radio border label="1">女</el-radio>
                            <el-radio border label="2">男</el-radio>
                        </el-radio-group>
                    </el-form-item>

                    <el-form-item label="出生日期" :label-width="formLabelWidth" prop="birthday">
                        <el-col :span="11">
                            <el-date-picker type="date" placeholder="选择日期" v-model="form.birthday" style="width: 100%;"></el-date-picker>
                        </el-col>
                    </el-form-item>

                    <el-form-item label="电话" :label-width="formLabelWidth" prop="phone">
                        <el-input placeholder="请输入联系电话" v-model="form.phone" autocomplete="off"></el-input>
                    </el-form-item>

                    <el-form-item label="地址" :label-width="formLabelWidth" prop="address">
                        <el-input placeholder="请输入地址" v-model="form.address" autocomplete="off"></el-input>
                    </el-form-item>

                    <el-form-item label="用户角色"  :label-width="formLabelWidth" prop="userRole">
                        <el-select v-model="form.userRole" placeholder="请选择角色">
                            <el-option label="系统管理员" value="1"></el-option>
                            <el-option label="经理" value="2"></el-option>
                            <el-option label="普通员工" value="3"></el-option>
                        </el-select>
                    </el-form-item>

                </el-form>

                <div slot="footer" class="dialog-footer">
                    <el-button @click="dialogFormVisible = false">取 消</el-button>
                    <el-button type="primary" @click="addUser('form')">确 定</el-button>
                </div>
            </el-dialog>
        </div>


        <!--账单表格 样式和供应商公用-->
        <table class="providerTable" cellpadding="0" cellspacing="0">
            <tr class="firstTr">
                <th width="10%">用户编码</th>
                <th width="10%">用户名称</th>
                <th width="10%">性别</th>

                <th width="10%">年龄</th>
                <th width="20%">电话</th>
                <th width="15%">角色</th>
                <th width="15%">操作</th>
            </tr>

            <c:forEach items="${userList}" var="list">
                <tr id="${list.id}">
                    <td>${list.userCode}</td>
                    <td>${list.userName}</td>

                    <td>
                        <c:choose>
                            <c:when test="${list.gender==1}">
                                女
                            </c:when>
                            <c:otherwise>男</c:otherwise>
                        </c:choose>
                    </td>

                    <td>
                        <c:set var="now"  value="<%=new java.util.Date()%>" />
                        <fmt:formatNumber type="number" pattern="##" value="${(((now.time)-(list.birthday.time))/1000/60/60/24/365)}" />
                    </td>

                    <td>
                        ${list.phone}
                    </td>
                    <td>
                        <c:choose>
                            <c:when test="${list.userRole == 1}">
                                系统管理员
                            </c:when>
                            <c:when test="${list.userRole == 2}">
                                经理
                            </c:when>
                            <c:otherwise>
                                普通员工
                            </c:otherwise>
                        </c:choose>
                    </td>
                    <td>
                        <a href="#" onclick="show(${list.id})" name="${list.id}"><img
                                src="${pageContext.servletContext.contextPath}/static/img/read.png" alt="查看"
                                title="查看"/></a>
                        <a href="billUpdate.html"><img
                                src="${pageContext.servletContext.contextPath}/static/img/xiugai.png" alt="修改"
                                title="修改"/></a>
                        <a href="#" class="removeBill"><img
                                src="${pageContext.servletContext.contextPath}/static/img/schu.png" alt="删除"
                                title="删除"/></a>
                    </td>
                </tr>
            </c:forEach>
        </table>
        当前${pages.pageNo}页/总共${pages.totalPageCount}页,共${pages.totalCount}条数据.
        <a href="${pageContext.servletContext.contextPath}/user/userList?pageNo=${pages.pageNo-1}&userName=${paramMap.userName}&userRole=${paramMap.userRole}">上一页</a>
        <a href="${pageContext.servletContext.contextPath}/user/userList?pageNo=${pages.pageNo+1}&userName=${paramMap.userName}&userRole=${paramMap.userRole}">下一页</a>
        <a href="${pageContext.servletContext.contextPath}/user/userList?pageNo=1&userName=${paramMap.userName}&userRole=${paramMap.userRole}">首页</a>
        <a href="${pageContext.servletContext.contextPath}/user/userList?pageNo=${pages.totalPageCount}&userName=${paramMap.userName}&userRole=${paramMap.userRole}">尾页</a>

        <div class="providerAdd" id="detail" style="display: none">
            <form action="##" id="UserDetail" onsubmit="return false">
                <div style="display: none">
                    <label for="id">用户Id：</label>
                    <input type="text" name="id" id="id"/>
                </div>

                <div class="">
                    <label for="userCode">用户编码：</label>
                    <input type="text" name="userCode" id="userCode"/>
                </div>
                <div>
                    <label for="userName">用户姓名：</label>
                    <input type="text" name="userName"  id="userName"/>

                </div>
                <div>
                    <label for="gender">用户性别：</label>
                    <select name="gender" id="gender" >
                        <option value="1">女</option>
                        <option value="2">男</option>
                    </select>
                </div>
                <div>
                    <label>出生日期：</label>
                    <input type="text" name="birthday" id="birthday"/>
                </div>
                <div>
                    <label>联系电话：</label>
                    <input type="text" name="phone" id="phone"/>
                    <%--                        <input type="text" path="" id="isPay" />--%>
                </div>
                <div>
                    <label>用户角色：</label>
                    <select name="userRole" id="userRole" >
                        <option value="1">系统管理员</option>
                        <option value="2">经理</option>
                        <option value="3">普通员工</option>
                    </select>
                </div>
                <div>
                    <label for="address">地址：</label>
                    <input name="address" id="address" value=""/>
                </div>

                <div class="providerAddBtn">
                    <!--<a href="#">保存</a>-->
                    <!--<a href="billList.html">返回</a>-->
                    <input type="button" id="update" value="保存"/>
                    <input type="button" value="返回" onclick="history.back(-1)"/>
                </div>
            </form>
        </div>
    </div>

</section>


<!--点击删除按钮后弹出的页面-->
<div class="zhezhao"></div>
<div class="remove" id="removeBi">
    <div class="removerChid">
        <h2>提示</h2>
        <div class="removeMain">
            <p>你确定要删除该订单吗？</p>
            <a href="#" id="yes">确定</a>
            <a href="#" id="no">取消</a>
        </div>
    </div>
</div>


<jsp:include page="comment/foot.jsp"></jsp:include>

<script>
    var Main = {
        data() {
            return {
                dialogTableVisible: false,
                dialogFormVisible: false,
                form: {
                    address:"",
                    birthday:"",
                    gender:"",
                    phone:"",
                    userCode:"",
                    userName:"",
                    userPassword:"",
                    userRole:"",
                },
                rules: {
                    userCode: [
                        { required: true, message: '请输入编码', trigger: 'blur' },
                    ],
                    userRole: [
                        { required: true, message: '请选择角色', trigger: 'change' }
                    ],
                    birthday: [
                        { type: 'date', required: true, message: '请选择日期', trigger: 'blur' }
                    ],
                    gender: [
                        { required: true, message: '请选择性别', trigger: 'change' }
                    ],
                    userPassword: [
                        { required: true, message: '请输入密码', trigger: 'change' }
                    ],
                    phone: [
                        { required: true, message: '请输入手机', trigger: 'blur' }
                    ],
                    address: [
                        { required: true, message: '请输入地址', trigger: 'blur' }
                    ],
                    userName: [
                        { required: true, message: '请输入姓名', trigger: 'blur' }
                    ]
                },
                formLabelWidth: '120px'
            };
        },
        methods:{
            addUser(form){
                var me=this;
                this.$refs[form].validate((valid) => {
                    if (valid) {
                        $.ajax({
                            type: "POST",   //提交的方法
                            url: "${pageContext.servletContext.contextPath}/user/userAdd", //提交的地址
                            contentType : 'application/json',
                            dataType : 'json',
                            data: JSON.stringify(this.form),// 序列化表单值
                            async: false,
                            success: function (data) {  //成功
                                alert("添加成功")
                                me.dialogFormVisible=false
                                me.form={};
                            },
                            error:function (data) {
                                alert("失败:"+data)
                            }
                        });
                    } else {
                        this.open3();
                        return false;
                    }
                });
                console.log(this.form)

            },
            open3() {
                this.$message({
                    showClose: true,
                    message: '警告哦，你的表单信息有误',
                    type: 'warning'
                });
            },
        }
    };
    var Ctor = Vue.extend(Main)
    new Ctor().$mount('#app')

    $(function () {

        var $isPay = $("#thisRole").val();
        var $option1 = $("[name='userRole']").find("option");
        for (var i = 0; i < $option1.length; i++) {
            if ($option1.eq(i).val() == $isPay) {
                $option1.eq(i).attr("selected", "true")
            }
        }


        $("#update").click(function () {
            var formData = $('#UserDetail').serialize();
            console.log(formData)
            $.ajax({
                type: "POST",   //提交的方法
                url:"${pageContext.servletContext.contextPath}/user/userUpdate", //提交的地址
                data:formData,// 序列化表单值
                async: false,
                error: function(request) {  //失败的话
                    alert("error")
                    console.log($('#update').serialize());
                },
                success: function(data) {  //成功
                    alert("修改成功")
                }
            });
        });
    });


    function show(id) {
        var proId = id;
        $.ajax({
            contentType: "application/json",
            url: "${pageContext.servletContext.contextPath}/user/userShow/" + proId,
            type: "get",
            dataType: "json",
            success: function (data) {
                $("#detail").show();
                $("#id").attr("value", data.id)
                $("#userCode").attr("value", data.userCode)
                $("#userName").attr("value", data.userName)
                $("#gender").val(data.gender)
                $("#birthday").attr("value", data.birthday)
                $("#phone").attr("value",data.phone)
                $("#userRole").val(data.userRole)
                $("#address").attr("value", data.address)
            }
        });
    }


</script>
</body>
</html>