<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>

当前${param.pageNo}页/总共${param.totalPageCount}页,共${param.totalCount}条数据.
<a href="${pageContext.servletContext.contextPath}/bill/billList?pageNo=${param.pageNo-1}&providerId=${param.providerId}&isPayment=${param.isPayment}&productName=${param.productName}">上一页</a>
<a href="${pageContext.servletContext.contextPath}/bill/billList?pageNo=${param.pageNo+1}&providerId=${param.providerId}&isPayment=${param.isPayment}&productName=${param.productName}">下一页</a>
<a href="${pageContext.servletContext.contextPath}/bill/billList?pageNo=1&providerId=${param.providerId}&isPayment=${param.isPayment}&productName=${param.productName}">首页</a>
<a href="${pageContext.servletContext.contextPath}/bill/billList?pageNo=${param.totalPageCount}&providerId=${param.providerId}&isPayment=${param.isPayment}&productName=${param.productName}">尾页</a>