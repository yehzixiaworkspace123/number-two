﻿<%@ include file="comment/jspHead.jsp" %>
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>超市账单管理系统</title>
    <link rel="stylesheet" href="${pageContext.servletContext.contextPath}/static/css/public.css"/>
    <link rel="stylesheet" href="${pageContext.servletContext.contextPath}/static/css/style.css"/>


</head>
<body>

<jsp:include page="comment/head.jsp"></jsp:include>

<!--主体内容-->
<section class="publicMian ">
    <jsp:include page="comment/left.jsp"></jsp:include>
    <div class="right">
        <div class="location">
            <strong>你现在所在的位置是:</strong>
            <span>供应商管理页面</span>
        </div>

        <form method="get" action="${pageContext.servletContext.contextPath}/provider/providerList">
            <div class="search">
                <span>供应商名称：</span>
                <input type="text" name="proName" value="${paramMap.proName}" placeholder="请输入供应商名称"/>


                <span>联系人：</span>
                <input type="text" name="proContact" value="${paramMap.proContact}" placeholder="请输入供应商联系人"/>

                <input type="submit" id="aa" value="查询"/>

            </div>
        </form>

        <div id="app">
            <!-- Form -->
            <el-button type="text" @click="dialogFormVisible = true">添加供应商</el-button>
            <el-dialog title="添加供应商" :visible.sync="dialogFormVisible">
                <el-form :model="form" >
                    <el-form-item label="供应商编号" :label-width="formLabelWidth">
                        <el-input v-model="form.proCode" autocomplete="off"></el-input>
                        <%--                            <input name="asd" id="asd" value=""/>--%>
                    </el-form-item>
                    <el-form-item label="供应商名称" :label-width="formLabelWidth">
                        <el-input v-model="form.proName" autocomplete="off"></el-input>
<%--                            <input name="asd" id="asd" value=""/>--%>
                    </el-form-item>

                    <el-form-item label="供应商描述" :label-width="formLabelWidth">
                        <el-input v-model="form.proDesc" autocomplete="off"></el-input>
                    </el-form-item>
                    <el-form-item label="供应商联系人" :label-width="formLabelWidth">
                        <el-input v-model="form.proContact" autocomplete="off"></el-input>
                    </el-form-item>
                    <el-form-item label="供应商联系电话" :label-width="formLabelWidth">
                        <el-input v-model="form.proPhone" autocomplete="off"></el-input>
                    </el-form-item>
                    <el-form-item label="地址" :label-width="formLabelWidth">
                        <el-input v-model="form.proAddress" autocomplete="off"></el-input>
                    </el-form-item>
                    <el-form-item label="传真" :label-width="formLabelWidth">
                        <el-input v-model="form.proFax" autocomplete="off"></el-input>
                    </el-form-item>

                </el-form>
                <div slot="footer" class="dialog-footer">
                    <el-button @click="dialogFormVisible = false">取 消</el-button>
                    <el-button type="primary" @click="addProviderInfo">确 定</el-button>
                </div>
            </el-dialog>
        </div>


        <!--账单表格 样式和供应商公用-->
        <table class="providerTable" cellpadding="0" cellspacing="0">
            <tr class="firstTr">
                <th width="10%">供应商编码</th>
                <th width="10%">供应商名称</th>
                <th width="10%">供应联系人</th>

                <th width="10%">联系电话</th>
                <th width="20%">地址</th>
                <th width="15%">创建时间</th>
                <th width="15%">操作</th>
            </tr>

            <c:forEach items="${providerList}" var="list">
                <tr id="${list.id}">
                    <td>${list.proCode}</td>
                    <td>${list.proName}</td>
                    <td>${list.proContact}</td>
                    <td>${list.proPhone}</td>
                    <td>
                            ${list.proAddress}
                    </td>
                    <td>
                        <fmt:formatDate value="${list.creationDate}" pattern="yyyy-MM-dd HH:mm:ss"></fmt:formatDate>
                    </td>
                    <td>
                        <a href="#" onclick="show(${list.id})" name="${list.id}"><img
                                src="${pageContext.servletContext.contextPath}/static/img/read.png" alt="查看"
                                title="查看"/></a>
                        <a href="billUpdate.html"><img
                                src="${pageContext.servletContext.contextPath}/static/img/xiugai.png" alt="修改"
                                title="修改"/></a>
                        <a href="#" class="removeBill"><img
                                src="${pageContext.servletContext.contextPath}/static/img/schu.png" alt="删除"
                                title="删除"/></a>
                    </td>
                </tr>
            </c:forEach>
        </table>
        当前${pages.pageNo}页/总共${pages.totalPageCount}页,共${pages.totalCount}条数据.
        <a href="${pageContext.servletContext.contextPath}/provider/providerList?pageNo=${pages.pageNo-1}&proName=${paramMap.proName}&proContact=${paramMap.proContact}">上一页</a>
        <a href="${pageContext.servletContext.contextPath}/provider/providerList?pageNo=${pages.pageNo+1}&proName=${paramMap.proName}&proContact=${paramMap.proContact}">下一页</a>
        <a href="${pageContext.servletContext.contextPath}/provider/providerList?pageNo=1&proName=${paramMap.proName}&proContact=${paramMap.proContact}">首页</a>
        <a href="${pageContext.servletContext.contextPath}/provider/providerList?pageNo=${pages.totalPageCount}&proName=${paramMap.proName}&proContact=${paramMap.proContact}">尾页</a>

        <div class="providerAdd" id="detail" style="display: none">
            <form action="##" id="ProviderDetail" onsubmit="return false">
                <!--div的class 为error是验证错误，ok是验证成功-->

                <div style="display: none">
                    <label for="id">供应商ID：</label>
                    <input type="text" name="id" id="id"/>
                </div>

                <div class="">
                    <label for="proCode">供应商编码：</label>
                    <input type="text" name="proCode" id="proCode"/>
                </div>
                <div>
                    <label for="proName">供应商名称：</label>
                    <input type="text" name="proName" path="" id="proName"/>

                </div>
                <div>
                    <label for="proDesc">供应商描述：</label>
                    <input type="text" name="proDesc" id="proDesc"/>
                </div>
                <div>
                    <label>供应商联系人：</label>
                    <input type="text" name="proContact" id="proContact"/>
                </div>
                <div>
                    <label>联系电话：</label>
                    <input type="text" name="proPhone" id="proPhone"/>
                    <%--                        <input type="text" path="" id="isPay" />--%>
                </div>
                <div>
                    <label>地址：</label>
                    <input name="proAddress" id="proAddress" value=""/>
                </div>
                <div>
                    <label for="proFax">传真：</label>
                    <input name="proFax" id="proFax" value=""/>
                </div>
                <div>
                    <label for="creationDate">创建时间：</label>
                    <input name="creationDate" id="createTime" value=""/>
                </div>
                <div class="providerAddBtn">
                    <!--<a href="#">保存</a>-->
                    <!--<a href="billList.html">返回</a>-->
                    <input type="button" id="update" value="保存"/>
                    <input type="button" value="返回" onclick="history.back(-1)"/>
                </div>
            </form>
        </div>
    </div>

</section>


<!--点击删除按钮后弹出的页面-->
<div class="zhezhao"></div>
<div class="remove" id="removeBi">
    <div class="removerChid">
        <h2>提示</h2>
        <div class="removeMain">
            <p>你确定要删除该订单吗？</p>
            <a href="#" id="yes">确定</a>
            <a href="#" id="no">取消</a>
        </div>
    </div>
</div>


<jsp:include page="comment/foot.jsp"></jsp:include>

<script>
    var Main = {
        data() {
            return {
                dialogTableVisible: false,
                dialogFormVisible: false,
                form: {
                    proCode:"",
                    proName: "",
                    proDesc: "",
                    proContact: "",
                    proPhone: "",
                    proAddress: "",
                    proFax: "",
                },
                formLabelWidth: '120px'
            };
        },
        methods:{
            addProviderInfo(){
                var me=this;
                if (this.form.proCode=="" || this.form.proName=="" || this.form.proDesc=="" || this.form.proContact=="" ||
                    this.form.proPhone=="" || this.form.proAddress=="" || this.form.proFax==""
                ){
                    this.open3();
                    return;
                }
                console.log(this.form)
                $.ajax({
                    type: "POST",   //提交的方法
                    url: "${pageContext.servletContext.contextPath}/provider/providerAdd", //提交的地址
                    contentType : 'application/json',
                    dataType : 'json',
                    data: JSON.stringify(this.form),// 序列化表单值
                    async: false,
                    success: function (data) {  //成功
                        alert("添加成功")
                        me.dialogFormVisible=false
                        me.form={};
                    },
                    error:function (data) {
                        alert("失败:"+data)
                    }
                });
            },
            open3() {
                this.$message({
                    showClose: true,
                    message: '警告哦，你的表单信息有误',
                    type: 'warning'
                });
            },
        }
    };
    var Ctor = Vue.extend(Main)
    new Ctor().$mount('#app')

    $(function () {
        var $proId = $("#providerId").val();
        var $isPay = $("#isPayment").val();
        var $option1 = $("[name='isPayment']").find("option");
        for (var i = 0; i < $option1.length; i++) {
            if ($option1.eq(i).val() == $isPay) {
                $option1.eq(i).attr("selected", "true")
            }
        }

        var $option2 = $("[name='providerId']").find("option");
        for (var j = 0; j < $option2.length; j++) {
            if ($option2.eq(j).val() == $proId) {
                $option2.eq(j).attr("selected", "true");
            }
        }

        $("#update").click(function () {
            var formData = $('#ProviderDetail').serialize();
            console.log(formData)
            $.ajax({
                type: "POST",   //提交的方法
                url:"${pageContext.servletContext.contextPath}/provider/providerUpdate", //提交的地址
                data:formData,// 序列化表单值
                async: false,
                error: function(request) {  //失败的话
                    alert("error")
                    console.log($('#update').serialize());
                },
                success: function(data) {  //成功
                    alert("修改成功")
                }
            });
        });
    });


    function show(id) {
        var proId = id;
        $.ajax({
            contentType: "application/json",
            url: "${pageContext.servletContext.contextPath}/provider/providerShow/" + proId,
            // data:JSON.stringify({"userCode":$user,"userPassword":$mima,birthday:"1992-12-12"}),
            type: "get",
            dataType: "json",
            success: function (data) {
                $("#detail").show();
                $("#proCode").attr("value", data.proCode)
                $("#proName").attr("value", data.proName)
                $("#proDesc").attr("value", data.proDesc)
                $("#proContact").attr("value", data.proContact)
                $("#proPhone").attr("value",data.proPhone)
                $("#proAddress").attr("value", data.proAddress)
                $("#proFax").attr("value", data.proFax)
                $("#createTime").attr("value", data.creationDate)
                $("#id").attr("value", data.id)
            }
        });
    }


</script>
</body>
</html>