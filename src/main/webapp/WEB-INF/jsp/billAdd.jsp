﻿<%@ include file="comment/jspHead.jsp"%>
<!--spring form-->
<%@ taglib prefix="fm" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>超市账单管理系统</title>
    <link rel="stylesheet" href="${pageContext.servletContext.contextPath}/static/css/public.css"/>
    <link rel="stylesheet" href="${pageContext.servletContext.contextPath}/static/css/style.css"/>
    <script src="${pageContext.servletContext.contextPath}/static/calendar/WdatePicker.js"></script>
</head>
<body>
<jsp:include page="comment/head.jsp"></jsp:include>
<!--主体内容-->
<section class="publicMian ">
    <jsp:include page="comment/left.jsp"></jsp:include>
    <div class="right">
        <div class="location">
            <strong>你现在所在的位置是:</strong>
            <span>账单管理页面 >> 订单添加页面</span>
        </div>
        <div class="providerAdd">

            <fm:form modelAttribute="bill" method="post" enctype="multipart/form-data">
                <!--div的class 为error是验证错误，ok是验证成功-->
                <div class="">
                    <label for="billId">订单编码：</label>
                    <fm:input type="text" path="billCode" name="billCode" id="billId" />
                    <span>*请输入订单编码</span>
                    <!--错误信息的显示-->
                    <fm:errors path="billCode"></fm:errors>
                </div>
                <div>
                    <label for="billName">商品名称：</label>
                    <fm:input type="text"  path="productName" id="billName" />
                    <span >*请输入商品名称</span>
                    <fm:errors path="productName"></fm:errors>
                </div>
                <div>
                    <label for="billCom">商品单位：</label>
                    <fm:input type="text"  path="productUnit" id="billCom" />
                    <span>*请输入商品单位</span>
                </div>
                <div>
                    <label for="billNum">商品数量：</label>
                    <fm:input type="text" path="productCount" id="billNum" />
                    <span>*请输入大于0的正自然数，小数点后保留2位</span>
                </div>
                <div>
                    <label for="money">总金额：</label>
                    <fm:input type="text" path="totalPrice" id="money" />
                    <span>*请输入大于0的正自然数，小数点后保留2位</span>
                </div>
                <div>
                    <label >供应商：</label>
                    <fm:select path="providerId" >
                        <fm:option value="">--请选择相应的提供商--</fm:option>
                        <fm:option value="1">北京三木堂商贸有限公司</fm:option>
                        <fm:option value="2">石家庄帅益食品贸易有限公司</fm:option>
                    </fm:select>
                    <span>*请选择供应商</span>
                </div>
                <div>
                    <label >是否付款：</label>
                    <fm:radiobutton path="isPayment" value="1"/>未付款
                    <fm:radiobutton path="isPayment" value="2"/>已付款
                </div>

                <div>
                    <label >账单照片：</label>
                    <input type="file" name="attachs">
                </div>


                <div>
                    <label >签订时间：</label>
                    <fm:input path="modifyDate" onclick="WdatePicker();"/>
                </div>

                <div class="providerAddBtn">
                    <!--<a href="#">保存</a>-->
                    <!--<a href="billList.html">返回</a>-->
                    <input type="submit" value="保存" />
                    <input type="button" value="返回" onclick="history.back(-1)"/>
                </div>
            </fm:form>
        </div>
    </div>
</section>

<jsp:include page="comment/foot.jsp"></jsp:include>
</body>
</html>