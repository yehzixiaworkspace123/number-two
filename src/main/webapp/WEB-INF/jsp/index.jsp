﻿<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>超市账单管理系统</title>
    <link rel="stylesheet" href="${pageContext.servletContext.contextPath}/static/css/public.css"/>
    <link rel="stylesheet" href="${pageContext.servletContext.contextPath}/static/css/style.css"/>
</head>
<body>

<jsp:include page="comment/head.jsp"></jsp:include>

<!--主体内容-->
<section class="publicMian">
   <jsp:include page="comment/left.jsp"></jsp:include>
    <div class="right">
        <img class="wColck" src="${pageContext.servletContext.contextPath}/static/img/clock.jpg" alt=""/>
        <div class="wFont">
            <h2>${sessionScope.user_session.userName}</h2>
            <p>欢迎来到超市账单管理系统!</p>
			<span id="hours"></span>
        </div>
    </div>
</section>
<jsp:include page="comment/foot.jsp"></jsp:include>
</body>
</html>