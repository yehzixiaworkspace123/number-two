﻿<%@ include file="comment/jspHead.jsp"%>
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>超市账单管理系统</title>
    <link rel="stylesheet" href="${pageContext.servletContext.contextPath}/static/css/public.css"/>
    <link rel="stylesheet" href="${pageContext.servletContext.contextPath}/static/css/style.css"/>
</head>
<body>

<jsp:include page="comment/head.jsp"></jsp:include>

<!--主体内容-->
    <section class="publicMian ">
        <jsp:include page="comment/left.jsp"></jsp:include>
        <div class="right">
            <div class="location">
                <strong>你现在所在的位置是:</strong>
                <span>账单管理页面</span>
            </div>
           <form method="get" action="${pageContext.servletContext.contextPath}/bill/billList">
            <div class="search">
                <span>商品名称：</span>
                <input type="text" name="productName" value="${paramMap.productName}" placeholder="请输入商品的名称"/>
                
                <span>供应商：</span>
                <select name="providerId" >
                    <option value="">--请选择--</option>
                    <option value="1">北京三木堂商贸有限公司</option>
                    <option value="2">石家庄帅益食品贸易有限公司</option>
                    <option value="3">深圳市泰香米业有限公司</option>
                    <option value="4">深圳市喜来客商贸有限公司</option>
                </select>

                <span>是否付款：</span>
                <select name="isPayment">
                    <option value="">--请选择--</option>
                    <option value="2">已付款</option>
                    <option value="1">未付款</option>
                </select>

                <input type="hidden" id="providerId" value="${paramMap.providerId}">
                <input type="hidden" id="isPayment" value="${paramMap.isPayment}">
                <input type="submit" id="aa" value="查询"/>
            </div>
           </form>

                <a href="${pageContext.servletContext.contextPath}/bill/billAdd">添加订单</a>

            <!--账单表格 样式和供应商公用-->
            <table class="providerTable" cellpadding="0" cellspacing="0">
                <tr class="firstTr">
                    <th width="10%">账单编码</th>
                    <th width="20%">商品名称</th>
                    <th width="20%">供应商</th>
                    <th width="10%">账单金额</th>
                    <th width="10%">是否付款</th>
                    <th width="15%">创建时间</th>
                    <th width="15%">操作</th>
                </tr>

                <c:forEach items="${billList}" var="list">
                <tr id="${list.id}">
                    <td>${list.billCode}</td>
                    <td>${list.productName}</td>
                    <td>${list.provider.proName}</td>
                    <td>${list.totalPrice}</td>
                    <td>
                    <c:choose>
                        <c:when test="${list.isPayment==1}">
                            未支付
                        </c:when>
                        <c:otherwise>已支付</c:otherwise>
                    </c:choose>
                    </td>
                    <td>
                        <fmt:formatDate value="${list.creationDate}" pattern="yyyy-MM-dd HH:mm:ss"></fmt:formatDate>
                     </td>
                    <td>
                        <a href="#" onclick="show(${list.id})" name="${list.id}" ><img src="${pageContext.servletContext.contextPath}/static/img/read.png" alt="查看" title="查看"/></a>
                        <a href="billUpdate.html"><img src="${pageContext.servletContext.contextPath}/static/img/xiugai.png" alt="修改" title="修改"/></a>
                        <a href="#" class="removeBill" name="${list.id}"><img src="${pageContext.servletContext.contextPath}/static/img/schu.png" alt="删除" title="删除"/></a>
                    </td>
                </tr>
                </c:forEach>
            </table>
            <jsp:include page="comment/page.jsp">
                <jsp:param name="pageSize" value="${pages.pageSize}"/>
                <jsp:param name="pageNo" value="${pages.pageNo}"/>
                <jsp:param name="totalCount" value="${pages.totalCount}"/>
                <jsp:param name="totalPageCount" value="${pages.totalPageCount}"/>


                <jsp:param name="providerId" value="${paramMap.providerId}"/>
                <jsp:param name="isPayment" value="${paramMap.isPayment}"/>
                <jsp:param name="productName" value="${paramMap.productName}"/>
            </jsp:include>

            <div class="providerAdd" id="detail" style="display: none">
                <form action="##"   id="billDetail" onsubmit="return false">
                    <!--div的class 为error是验证错误，ok是验证成功-->
                    <div class="">
                        <label for="billId">订单编码：</label>
                        <input type="text" path="" name="billCode" id="billId" />
                        <!--错误信息的显示-->
                        <errors path="billCode"></errors>
                    </div>
                    <div>
                        <label for="billName">商品名称：</label>
                        <input type="text" name="productName" path="" id="billName" />
                    </div>
                    <div>
                        <label >供应商：</label>
                        <input type="text" name="providerName" id="providerName" />
                    </div>
                    <div>
                        <label>账单金额：</label>
                        <input type="text" name="totalPrice" id="billMoney" />
                    </div>
                    <div>
                        <label >是否付款：</label>
                        <select name="isPayment" id="isPay" >
                            <option value="2">已付款</option>
                            <option value="1">未付款</option>
                        </select>
<%--                        <input type="text" path="" id="isPay" />--%>
                    </div>
                    <div>
                        <label >创建时间：</label>
                        <input  name="creationDate" id="createTime" value=""/>
                    </div>
                    <div class="providerAddBtn">
                        <!--<a href="#">保存</a>-->
                        <!--<a href="billList.html">返回</a>-->
                        <input type="button" id="update" value="保存" />
                        <input type="button" value="返回" onclick="history.back(-1)"/>
                    </div>
                </form>
            </div>
        </div>

    </section>


<!--点击删除按钮后弹出的页面-->
<div class="zhezhao"></div>
<div class="remove" id="removeBi">
    <div class="removerChid">
        <h2>提示</h2>
        <div class="removeMain">
            <input id="tempId" value="" style="display:none;">
            <p>你确定要删除该订单吗？</p>
            <a href="#" id="yes">确定</a>
            <a href="#" id="no">取消</a>
        </div>
    </div>
</div>




<jsp:include page="comment/foot.jsp"></jsp:include>

<script>

    $(function () {
        var $proId = $("#providerId").val();
        var $isPay = $("#isPayment").val();
        var $option1 = $("[name='isPayment']").find("option");
        for(var i =0;i<$option1.length;i++){
           if($option1.eq(i).val()==$isPay){
               $option1.eq(i).attr("selected","true")
           }
        }

        var $option2 = $("[name='providerId']").find("option");
        for(var j = 0;j<$option2.length;j++){
            if($option2.eq(j).val()==$proId){
                $option2.eq(j).attr("selected","true");
            }
        }

        $("#update").click(function () {
            var formData=$('#billDetail').serialize()
            console.log(formData)
            $.ajax({
                type: "POST",   //提交的方法
                url:"${pageContext.servletContext.contextPath}/bill/billUpdate", //提交的地址
                data:formData,// 序列化表单值
                async: false,
                error: function(request) {  //失败的话
                    alert("error")
                    console.log($('#update').serialize());
                },
                success: function(data) {  //成功
                    alert("修改成功")
                }
            });
        });

        $("#yes").click(function () {
            var id = $("#tempId").attr("value")
            alert("id:"+id)
            <%--$.ajax({--%>
            <%--    type: "POST",   //提交的方法--%>
            <%--    url:"${pageContext.servletContext.contextPath}/bill/billDelete", //提交的地址--%>
            <%--    data:id,// 序列化表单值--%>
            <%--    async: false,--%>
            <%--    error: function(request) {  //失败的话--%>
            <%--        alert("error")--%>
            <%--    },--%>
            <%--    success: function(data) {  //成功--%>
            <%--    }--%>
            <%--});--%>
        });

        });



    function show(id){
        var BillId=id;
        $.ajax({
            contentType:"application/json",
            url:"${pageContext.servletContext.contextPath}/bill/billShow/"+BillId,
            // data:JSON.stringify({"userCode":$user,"userPassword":$mima,birthday:"1992-12-12"}),
            type:"get",
            dataType:"json",
            success:function (data) {
                $("#detail").show();
                $("#billId").attr("value",data.billCode)
                $("#billName").attr("value",data.productName)
                $("#providerName").attr("value",data.provider.proName)
                $("#billMoney").attr("value",data.totalPrice)
                $("#isPay").val(data.isPayment)
                $("#createTime").attr("value",data.creationDate)
            }
        });
    }


    



</script>
</body>
</html>