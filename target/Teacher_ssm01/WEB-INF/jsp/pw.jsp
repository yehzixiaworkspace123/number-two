﻿<%@ include file="comment/jspHead.jsp"%>
<!--spring form-->
<%@ taglib prefix="fm" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>超市账单管理系统</title>
    <link rel="stylesheet" href="${pageContext.servletContext.contextPath}/static/css/public.css"/>
    <link rel="stylesheet" href="${pageContext.servletContext.contextPath}/static/css/style.css"/>
    <script src="${pageContext.servletContext.contextPath}/static/calendar/WdatePicker.js"></script>

    <style>
        .pw{
            border: 1px solid #9ac70f;
            padding: 20px;
            margin: 20px;
        }
        #app{
            width: 50%;
        }
    </style>

</head>
<body>
<jsp:include page="comment/head.jsp"></jsp:include>
<!--主体内容-->
<section class="publicMian ">
    <jsp:include page="comment/left.jsp"></jsp:include>
    <div class="right">
        <div class="location">
            <strong>你现在所在的位置是:</strong>
            <span>密码修改页面</span>
        </div>
        <div class="pw" id="pw">
        <div id="app">
            <el-form :model="ruleForm" status-icon :rules="rules" ref="ruleForm" label-width="100px" class="demo-ruleForm">

                <el-form-item label="原密码" prop="oldPass">
                    <el-input v-model="ruleForm.oldPass" autocomplete="off"></el-input>
                </el-form-item>
                <el-form-item label="新密码" prop="pass">
                    <el-input type="password" v-model="ruleForm.pass" autocomplete="off"></el-input>
                </el-form-item>
                <el-form-item label="确认密码" prop="checkPass">
                    <el-input type="password" v-model="ruleForm.checkPass" autocomplete="off"></el-input>
                </el-form-item>

                <el-form-item>
                    <el-button type="primary" @click="submitForm('ruleForm')">提交</el-button>
                    <el-button @click="resetForm('ruleForm')">重置</el-button>
                </el-form-item>
            </el-form>
        </div>
        </div>
    </div>
</section>

<jsp:include page="comment/foot.jsp"></jsp:include>
</body>
</html>
<script>
    var Main = {
        data() {
            var checkOldPass = (rule, value, callback) => {
                if (!value) {
                    return callback(new Error('原密码不能为空'));
                }
                callback();
            };
            var validatePass = (rule, value, callback) => {
                if (value === '') {
                    callback(new Error('请输入密码'));
                } else {
                    if (this.ruleForm.checkPass !== '') {
                        this.$refs.ruleForm.validateField('checkPass');
                    }
                    callback();
                }
            };
            var validatePass2 = (rule, value, callback) => {
                if (value === '') {
                    callback(new Error('请再次输入密码'));
                } else if (value !== this.ruleForm.pass) {
                    callback(new Error('两次输入密码不一致!'));
                } else {
                    callback();
                }
            };
            return {
                ruleForm: {
                    pass: '',
                    checkPass: '',
                    oldPass: ''
                },
                rules: {
                    pass: [
                        { validator: validatePass, trigger: 'blur' }
                    ],
                    checkPass: [
                        { validator: validatePass2, trigger: 'blur' }
                    ],
                    oldPass: [
                        { validator: checkOldPass, trigger: 'blur' }
                    ]
                }
            };
        },

        methods: {
            submitForm(formName) {
                var me = this
                this.$refs[formName].validate((valid) => {
                    if (valid) {
                        console.log(JSON.stringify(me.ruleForm));
                        $.ajax({
                            type: "POST",   //提交的方法
                            url: "${pageContext.servletContext.contextPath}/index/updatePW", //提交的地址
                            contentType : 'application/json',
                            dataType : 'json',
                            data: JSON.stringify(me.ruleForm),// 序列化表单值
                            async: false,
                            success: function (data) {  //成功
                                if (data==200){
                                    alert("添加成功,请重新登录")
                                    window.location.href = "login";
                                }

                            },
                            error:function () {
                                alert("失败")
                                // window.location.href = "login";
                            }
                        });
                    } else {
                        console.log('error submit!!');
                        return false;
                    }
                });
            },
            resetForm(formName) {
                this.$refs[formName].resetFields();
            }
        }
    }
    var Ctor = Vue.extend(Main)
    new Ctor().$mount('#app')
</script>