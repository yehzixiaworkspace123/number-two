<%--
  Created by IntelliJ IDEA.
  User: demo
  Date: 2020/11/17
  Time: 9:51 上午
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<div class="left">
    <h2 class="leftH2"><span class="span1"></span>功能列表 <span></span></h2>
    <nav>
        <ul class="list">
            <li ><a href="${pageContext.servletContext.contextPath}/bill/billList">账单管理</a></li>
            <li><a href="${pageContext.servletContext.contextPath}/provider/providerList">供应商管理</a></li>
            <li><a href="${pageContext.servletContext.contextPath}/user/userList">用户管理</a></li>
            <li><a href="${pageContext.servletContext.contextPath}/index/pw">密码修改</a></li>
            <li><a href="${pageContext.servletContext.contextPath}/index/loginOut">退出系统</a></li>
        </ul>
    </nav>

</div>